from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.shortcuts import render , get_object_or_404
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm

@login_required
def list_projects(request):
    #projects_obj= Project.objects.all()
    projects_obj = Project.objects.filter(owner=request.user)
    #projects_obj.tasks_count = projects_obj.tasks.count()
    context = {'context':projects_obj}
    return render (request, 'base.html', context)

@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, 'projects/project_details.html', {'project': project})

def create_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_projects')
    else:
        form = ProjectForm()

    return render(request, 'projects/create_project.html', {'form': form})
